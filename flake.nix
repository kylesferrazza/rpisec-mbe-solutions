{
  description = "rpisec-mbe-solutions";
  outputs = { self, nixpkgs }: let
    pkgs = import nixpkgs {
      system = "x86_64-linux";
    };
  in {
    devShell.x86_64-linux = pkgs.mkShell {
      name = "rpisec-mbe-solutions";
      buildInputs = with pkgs; [
        rustc
        cargo
        rust-analyzer
        clippy
        gcc
      ];
    };
  };
}
